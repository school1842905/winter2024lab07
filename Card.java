public class Card {
    private int rank;
    private String suit;

    public Card(int rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public int getRank() {
        return this.rank;
    }

    public String getSuit() {
        return this.suit;
    }

    // not sure how to delete items from an array, so thought it'd be cool to change the rank to clarify if the card is 'out'
    public void setRank(int rank) {
        this.rank = rank;
    }

    public String toString() {
        String s = "";
        
		s = rank + " of " + suit + "\n";
        
        return s;
    }
	
	public double calculateScore() {
		double score = rank;
		
		if (suit.equals("Hearts")) {
			score += 0.4;
		}
		else if (suit.equals("Spades")) {
			score += 0.3;
		}
		else if (suit.equals("Diamonds")) {
			score += 0.2;
		}
		else if (suit.equals("Clubs")) {
			score += 0.1;
		}
		else {
			score = -1;
		}
		
		return score;
		
	}


}
