public class SimpleWar {
	public static void main(String[] args) {
		Deck stack = new Deck();
		
		stack.shuffle();
		
		//System.out.println(stack);
		
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		
		while (stack.numberOfCards > 0) {
			int round = 1;
			
			System.out.println("Round:" + round); // this doesnt work but its fine for now :)
			System.out.println("-------------------");
			
			// draw top card for each player
			Card cardOne = stack.drawTopCard();
			Card cardTwo = stack.drawTopCard();
			
			System.out.println("P1 card: " + cardOne);
			System.out.println("P2 card: " + cardTwo);
			
			// calculateScore for each player
			double playerOneScore = cardOne.calculateScore();
			double playerTwoScore = cardTwo.calculateScore();
			
			System.out.println("P1 Score: " + playerOneScore);
			System.out.println("P2 Score: " + playerTwoScore + "\n");
			
			System.out.println("P1 Points: : " + playerOnePoints);
			System.out.println("P1 Points: " + playerTwoPoints + "\n");
			
			
			// see who has the higher score and add a point for them
			if (playerOneScore > playerTwoScore) {
				System.out.println("Card one wins!\n");
				playerOnePoints++;
			}
			else if (playerTwoScore > playerOneScore) {
				System.out.println("Card two wins!\n");
				playerTwoPoints++;
			}
			else {
				System.out.println("It was a tie!\n");
			}
			
			System.out.println("P1 Points: : " + playerOnePoints);
			System.out.println("P1 Points: " + playerTwoPoints + "\n");
			
			System.out.println("-------------------\n");
			
			round++;
			
		}
		
		// print the winner
		if(playerOnePoints > playerTwoPoints) {
			System.out.println("Congrats! Player One Wins!");
		}
		else if (playerTwoPoints > playerOnePoints) {
			System.out.println("Congrats! Player Two Wins!");
		}
		else {
			System.out.println("Congrats! It's a tie");
		}
	}
	
}


